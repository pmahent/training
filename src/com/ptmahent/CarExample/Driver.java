package com.ptmahent.CarExample;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ptmahent on 4/30/2015.
 */
public class Driver {
    public String driverId = null;
    public String name = null;

    public List<Vehicle> vehicles = new ArrayList<Vehicle>();

    public String toString() {
        return this.driverId + " : " +
                this.name + " : " +
                this.vehicles;
    }
}
