package com.ptmahent.CarExample;

/**
 * Created by ptmahent on 4/30/2015.
 */
public class Vehicle {
    public String vehicleId = null;
    public String name = null;

    public String toString() {
        return this.vehicleId + " : " +
                this.name;
    }
}
