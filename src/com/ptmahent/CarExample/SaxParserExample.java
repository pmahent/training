package com.ptmahent.CarExample;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created by ptmahent on 4/30/2015.
 */
public class SaxParserExample {

    public static void main(String argv[]) {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        File dir1 = new File(".");
        File dir2 = new File("..");
        try {
            System.out.println("Current dir : " + dir1.getCanonicalPath());
            System.out.println("Parent  dir : " + dir2.getCanonicalPath());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            InputStream xmlInput = null;

            xmlInput = new FileInputStream("C:\\Users\\ptmahent\\IdeaProjects\\Sax Example\\src\\com\\ptmahent\\CarExample\\data\\sax-example.xml");


            SAXParser saxParser = factory.newSAXParser();
            SaxHandler handler = new SaxHandler();
            saxParser.parse(xmlInput, handler);

            for (Driver driver : handler.drivers) {
                System.out.println(driver);
            }

        } catch (Throwable err) {
            err.printStackTrace();
        }
    }
}
